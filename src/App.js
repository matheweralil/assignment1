import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {FriendList} from './components/FriendList';
import {Child} from './components/Child';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter,
  Switch
} from "react-router-dom";

/*
 * class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
         Lets get started.
        </p>
      </div>
    );
  }
}*/
//
export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      children: [
        {
          name: "paul",
          work: "soft",
          phone: "339939",
          email: "mathew@paqs",
          city: "nepal",
          no: 1,
          important: true
        }, {
          name: "mathew",
          work: "engin",
          phone: "3039",
          email: "roh@paqs",
          city: "cochin",
          no: 2,
          important: false
        }
      ],
      count: 2
    };
    const oldCount = this.state.count;
    this.userData = this.userData.bind(this);
    this.editData = this.editData.bind(this);
  }
  userData(newData) {
    //console.log("FriendList userData ");
    //console.log(newData);
    this.setState({
      children: [
        ...this.state.children,
        newData
      ],
      count: this.state.count + 1
    });
    //console.log("hello i am ready for a redirect");

  }
  editData(e){
    //console.log(e);
    let arr = this.state.children;
    // let newArray = [];
    arr.map((col) => {
      //console.log(col.name + " ," + e.name);
      if (col.no === e.no) {
        //console.log(col.name + "==" + e.name);
        //console.log("i am deleting this ");
        let index = arr.indexOf(col);
        //console.log(index + " of the obj toi mark as important");
        arr.splice(index, 1,e);
      } else {
        //console.log(col.name + "!==" + e.name);
      }
    });
    //console.log(arr);
    // arrayToDel.slice(arrayToDel,e.target.value);
    this.setState({children: arr, count: arr.length})
  }


  render() {
    // return (<div><FriendList/></div>);

    return (<Router >
      <div>
        <h2>React Assignment One using Router</h2>
        <ul>
          <li>
            <Link to={'/home'}>Home</Link>
          </li>
          <li>
            <Link to={'/Add'}>Add</Link>
          </li>

        </ul>
        <hr/>

        <Switch>
          <Route exact path='/home' main={<h2>HOME</h2>} sidebar={<div>Home</div>} render={props => <FriendList history={props.history}   newUser={this.state} {...props} />}/>
          <Route exact path='/Add' sidebar={<div>Add/Edit</div>}  main={<h2>Add/EDIT</h2>} component={props => <Child history={props.history} count={this.state.count} updateData={this.userData}  editData={this.editData} />}/>
          <Route exact path='/' sidebar={<div>Home</div>}  main={<h2>HOME</h2>} render={props => <FriendList history={props.history} editUser={this.editMe}  newUser={this.state} {...props} />}/>
        </Switch>
      </div>
    </Router>);
  }
}
export const AppH1 = (props) => {
  return (<div >
    <h1>
      Hello this is the {props.src}
      stuff.
    </h1>
  </div>);
}

//
// export class App extends React.Component {
//   render(){
//     return (<Router><li><NavLink to="/about">About</NavLink></li>
//             <li><NavLink to="/repos">Repos</NavLink></li>
//           </Router>);
//   }
// }
//export default App;
