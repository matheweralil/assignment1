import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
export class NavLink extends React.Component{
  render() {
    return <Link {...this.props} activeClassName="active"/>
  }
}
export default NavLink;
