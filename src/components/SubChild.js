import React from 'react';
//import PropTypes from 'prop-types';
export class SlNo extends React.Component {

  render() {
    return <td></td>;
  }

}

export class ClientName extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    this.props.onChange(e.target.value);
  }
  render() {
    return <td><input type="text" placeholder="name" value={this.props.name} onChange={this.handleChange}/></td>;
  }
}

export class Work extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    this.props.onChange(e.target.value);
  }
  render() {
    return <td><input type="text" placeholder="work"  value={this.props.work} onChange={this.handleChange}/></td>;
  }

}
export class PhoneNumber extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    this.props.onChange(e.target.value);
  }
  render() {
    return <td><input type="text" placeholder="phone"   value={this.props.phone}  onChange={this.handleChange}/></td>;
  }

}
export class Email extends React.Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    this.props.onChange(e.target.value);
  }
  render() {
    return <td><input type="text" placeholder="email"  value={this.props.email} onChange={this.handleChange}/></td>;
  }

}
export class City extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    this.props.onChange(e.target.value);
  }
  render() {
    return <td><input type="text" placeholder="city"  value={this.props.city} onChange={this.handleChange}/></td>;
  }
}
