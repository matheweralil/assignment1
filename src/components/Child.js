import React from 'react';
import {SlNo,ClientName,Work,PhoneNumber,Email,City} from './SubChild';
import {

  Redirect,withRouter

} from "react-router-dom";

export class Child extends React.Component {
  constructor(props) {
    super(props);
    console.log("in chhild");
    console.log(this.props);
    this.state={
      children: this.props.children,
      count: this.props.count
    };
    this.updateMe = this.updateMe.bind(this);

  }
  updateMe(newChild){
    //console.log("in child ,new Count "+newChild);
    if(this.props.history.location.state === undefined){
    console.log(this.props);
     this.props.updateData(newChild);
   }else{
       this.props.editData(newChild);
     }
     this.props.history.push('/home');


  }
  render(){
    // //console.log(this.props);
    return <Children count={this.props.count} toEdit={this.props.history.location.state} updateChild={this.updateMe}/>;

  }
}


class Children extends React.Component {
  constructor(props) {

    super(props);
//console.log("in children");
  //console.log(this.props);
    this.state={
      city: this.props.toEdit!== undefined?this.props.toEdit.city:"",
      email: this.props.toEdit!== undefined?this.props.toEdit.email:"",
      no: this.props.toEdit !== undefined?this.props.toEdit.no:this.props.count+1,
      phone: this.props.toEdit!== undefined?this.props.toEdit.phone :"",
      work: this.props.toEdit!== undefined?this.props.toEdit.work :"",
      name: this.props.toEdit!== undefined?this.props.toEdit.name :"",
      count: this.props.toEdit!== undefined?this.props.toEdit.count :this.props.count,
      important: this.props.toEdit!== undefined?this.props.toEdit.important :false

    };

    this.nameChange  = this.nameChange.bind(this);

    this.emailChange = this.emailChange.bind(this);
    this.cityChange	 = this.cityChange.bind(this);

    this.workChange  = this.workChange.bind(this);
    this.phChange    = this.phChange.bind(this);
    this.addChild = this.addChild.bind(this);

  }
  addChild(){
    //console.log(this.state);

      this.props.updateChild(this.state);
      this.setState({
        city: "",
        email: "",
        no: this.props.count+1,
        phone: "",
        work: "",
        name: "",
        count: this.props.count,
      });


  }
  nameChange(newName){
    console.log(newName);
    this.setState({
      name:newName
    });
  }
  workChange(newWork){
    //console.log("new work is "+newWork);
    this.setState({
      work:newWork
    });
  }
  phChange(newPh){
    //console.log("phone is "+newPh);
    this.setState({
      phone:newPh
    });
  }
  emailChange(newEmail){
    this.setState({
      email:newEmail
    });
  }
  cityChange(newCity){
    this.setState({
      city:newCity
    });
  }
  render() {

    return   (<table>
                <tbody>
                  <tr key={this.props.count}>
                    <SlNo no={this.state.no} />
                    <ClientName name={this.state.name} onChange={this.nameChange}/>
                    <PhoneNumber phone={this.state.phone} onChange={this.phChange}/>
                    <Work work={this.state.work} onChange={this.workChange}/>
                    <Email email={this.state.email} onChange={this.emailChange}/>
                    <City city={this.state.city} onChange={this.cityChange}/>
                    <td>
                      <button onClick={this.addChild}>Add</button>
                    </td>
                  </tr>

                </tbody>

              </table>

            );
  }
}
