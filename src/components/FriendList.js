import React from 'react';
import {Header} from './Header';
import {Child} from './Child';
import {ListChildren} from './ListChildren';
//import PropTypes from 'prop-types';
import {
  Redirect,withRouter
} from "react-router-dom";
export class FriendList extends React.Component {
  constructor(props) {
    super(props);
    let cols = [
      "SlNo",
      "Name",
      "Phone",
      "Work",
      "Email",
      "City",
      "Action"
    ];
    let styles = {
      padding: "2%",
      width: "90%",
      textAlign: "center"
    };
    //console.log(props);
    let appState = props.location.state;
    //console.log(appState);
    this.state = {
      colNames: cols,
      style: styles,
      children: this.props.newUser.children,
      count: 2
    };
    this.markAsImportant = this.markAsImportant.bind(this);
    this.userData = this.userData.bind(this);
    this.deleteMe = this.deleteMe.bind(this);
    this.update = this.update.bind(this);
  }

  userData(newData) {
    //console.log("FriendList userData ");
    //console.log(newData);
    this.setState({
      children: [
        ...this.state.children,
        newData
      ],
      count: this.state.count + 1
    });
  }
  update(e) {
    //console.log("FriendList update/edit func")
    //console.log(e);
      this.props.history.push({pathname:'/Add',state: e});
    // return <Redirect to="/Add"  toEdit={e} updateChild={this.updateMe}/>;
  }
  deleteMe(e) {
    let arrayToDel = this.state.children;


    let arr = this.state.children;
    // let newArray = [];
    arr.map((col) => {
      //console.log(col.name + " ," + e.name);
      if (col === e) {
        //console.log(col.name + "==" + e.name);
        //console.log("i am deleting this ");
        let index = arr.indexOf(col);
        arr.splice(index, 1);
      } else {
        //console.log(col.name + "!==" + e.name);
      }
    });
    //console.log(arr);
    // arrayToDel.slice(arrayToDel,e.target.value);
    this.setState({children: arr, count: arr.length})
  }
  markAsImportant(e) {
    //console.log("FriendList markas important func")
    //console.log(e);

    let arr = this.state.children;
    // let newArray = [];
    arr.map((col) => {
      //console.log(col.name + " ," + e.name);
      if (col === e) {
        //console.log(col.name + "==" + e.name);
        //console.log("i am marking this ");
        let oldVal = col.important;
        let index = arr.indexOf(col);
        //console.log(index + " of the obj toi mark as important" + oldVal);

        let newObj = {
          name: col.name,
          work: col.work,
          phone: col.phone,
          email: col.email,
          city: col.city,
          no: col.no,
          important: oldVal == false
            ? true
            : false
        };
        arr.splice(index, 1, newObj);
        // arr = [...arr,newObj];
      } else {
        //console.log("not marked");
      }
    });
    //console.log(arr);
    // arrayToDel.slice(arrayToDel,e.target.value);
    this.setState({children: arr})

  }
  render() {
    return (
      <table style={this.state.style}>
      <tbody>
        <Header cols={this.state}/>
        <ListChildren onList={this.state} onUpdateMe={this.update} onMarAsImportant={this.markAsImportant} onDelete={this.deleteMe}/>
      </tbody>
    </table>);
  }
}
