import React from 'react';

export class Header extends React.Component {
  showTds() {
    return this.props.cols.colNames.map(function(col, i) {
      // console.log(col);
      return <TablColumn key={"head_"+i.toString()} colName={col} />;
    });
  }

  render() {
    return <tr>{this.showTds()}</tr>;
  }
}
class TablColumn extends React.Component {

  render() {
    return <th style={this.props.style} key={this.props.id}>{this.props.colName}</th>;
  }
}
