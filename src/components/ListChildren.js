import React from 'react';
import {DeleteButton} from './DeleteButton';

export class ListChildren extends React.Component {
  constructor(props) {
    super(props);
    //console.log(this.props);
    //console.log("i am in list children");
    // const childrenArr = this.props.onList.children;
    // this.clickMe = this.clickMe.bind(this);
    // this.delete = this.delete.bind(this);
    // this.state={
    //   children: this.props.onList.children
    // };

  }
  componentWillReceiveProps(nextProps) {
    //console.log("i am in componentWillReceiveProps");
    //console.log(nextProps);
    let child = this.props.onList.children;

    // this.setState({
    //     children: [...this.state.children,nextProps]
    //   });

  }
  updateMe = (param) => (e) => {
    //console.log("You are going to be updated bro"); // the React Component instance
    //console.log(param);
    this.props.onUpdateMe(param);
  }
  markMe = (param) => (e) => {
    //console.log("You are marked"); // the React Component instance
    //console.log(param);
    this.props.onMarAsImportant(param);
  }

  clickMe = (param) => (e) => {
    //console.log("this is the beginning of the end bae"); // the React Component instance
    //console.log(param);
    // this.setState({
    //   deleteObj: this
    // });
    // //console.log(this.state);
    this.props.onDelete(param);
  }
  render() {
    return this.props.onList.children.map((col, i) => {
      // //console.log(col)
      // //console.log(' index=' + i);
      return (<tr key={i.toString()} style={col.important == false
          ? {
            fontWeight: 'normal'
          }
          : {
            fontWeight: 'bold'
          }}>

        <td>
          {i + 1}
        </td>
        <td>
          {col.name}
        </td>
        <td>
          {col.phone}
        </td>
        <td>
          {col.work}
        </td>
        <td>
          {col.email}
        </td>
        <td>
          {col.city}
        </td>
        <td>
          <button style={{
              color: "purple",
              background: "lightgrey"
            }} value={col.no} onClick={this.clickMe(col)}>&#x26D4;</button >
          <button style={{
              color: "green",
              background: "lightgrey"
            }} value={col.no} onClick={this.markMe(col)}><MarkSybmol importance={col.important} /></button>
            <button style={{
                color: "green",
                background: "lightgrey"
              }} value={col.no} onClick={this.updateMe(col)}>Edit</button>
        </td>
      </tr>);
    });
  }
}
class MarkSybmol extends React.Component{
  constructor(props){
    super(props);
    //console.log(this.props);
  }
  render(){

    if(this.props.importance == true ){
      return <div>&#x2605;</div>;
    }else {
      return <div>&#x2606;</div>;
    }
  }
}
