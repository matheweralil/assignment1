import React from 'react';


export class DeleteButton extends React.Component{
  constructor(props) {
    super(props);
    console.log(this.props);
    this.state={
      rownum: 0
    };
    this.deleteMe = this.deleteMe.bind(this);
    console.log("i am in delete button");
  }
  deleteMe(){
    this.props.deleteThis(this.props.index);
    // console.log(ev);
  }

  render(){
    return  (
    <button key={this.props.index} onClick={this.deleteMe}>Delete</button>
    );
  }
}
